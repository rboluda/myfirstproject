# Mi primera CNN

import tensorflow as tf
import numpy as np

# Dadtos que van a ser usados para entrenar la CNN
celsius = np.array([-40,-10,0,8,15,22,38],dtype=float)
fahrenheit = np.array([-40,-14,32,46,59,72,100],dtype=float)

# Vamos a usar Keras donde especificamos una única capa de salida
# capa es de tipo densa donde units es una única neurona
capa = tf.keras.layers.Dense(units=1,input_shape=[1])
modelo = tf.keras.Sequential([capa])

modelo.compile(
    optimizer = tf.keras.optimizers.Adam(0.1),
    loss = 'mean_squared_error'
)

print("Comenzamos entrenamiento...")
historial = modelo.fit(celsius, fahrenheit, epochs=1000, verbose=False)
print("Modelo entrenado completo desde macbook pro")